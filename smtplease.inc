<?php
/**
 * @file
 * Mail subsystem replacement functions for the SMTPlease module.
 */


// Load the PEAR Mail library.
require_once 'Mail.php';

/**
 * Implements drupal_mail_wrapper()
 */
function drupal_mail_wrapper($message) {
  global $user;

  // Load the module configuration settings.
  $settings = variable_get('smtplease_settings', array());

  // Make sure headers contain necessary details
  $message['headers']['Date']     = date("r");
  $message['headers']['Subject']  = $message['subject'];
  $message['headers']['To']       = $message['to'];
  $message['headers']['From']     = $message['from'];
  $message['headers']['Reply-To'] = $message['from'];

  // Unset default headers that we don't need and that probably aren't valid.
  unset($message['headers']['Return-Path']);
  unset($message['headers']['Errors-To']);
  unset($message['headers']['Sender']);

  // Queue or send mail depending on configuration.
  if (!empty($settings['QueueMail'])) {
    smtplease_queue_mail($message);
    return TRUE;
  }
  else {
    return smtplease_send_mail($message, $user->uid);
  }
}

/**
 * Process outgoing mail queue.
 *
 * @param $id
 *   (optional) The mail queue id of a specific message to send
 *   If not specified, the queue will be processed chronologically
 */
function _smtplease_worker_thread($id = 0) {
  $sent_count = 0;

  // Validate the message id, if provided.
  if (!is_numeric($id)) {
    return;
  }

  // Record that we having an active worker thread.
  cache_set('smtplease_running', TRUE);

  // Load the maximum outgoing message settings per process.
  $settings = variable_get('stmplease_settings', array());
  $batch_size = empty($settings['batch_size']) ? 20 : $settings['batch_size'];

  // Don't allow Batch Size to be set to zero in order to avoid infinite 
  // looping.
  if ($batch_size == 0) {
    $batch_size == 1;
  }

  // Perform mail queue processing.
  $mail_data = _smtplease_get_next_message($id);
  while (!empty($mail_data)) {
    ++$sent_count;

    // If the process send limit has been reached, spawn a new worker thread.
    // Only spawn a new thread if multi-threading is enabled, as this function
    // is also called on Cron runs.
    if ($sent_count > $batch_size) {
      if (!empty($settings['MultiThreading'])) {
        smtplease_spawn_thread();
      }

      // Exit the current worker thread.
      return;
    }

    // Prepare the message data for sending.
    $message = unserialize($mail_data['message_data']);

    // Send the next mail message in the queue.  
    if (smtplease_send_mail($message, $mail_data['uid'])) {
      $mail_data['processed'] = time();
      drupal_write_record('smtplease_queue', $mail_data, 'id');
    }

    // Check for more messages.
    $mail_data = _smtplease_get_next_message();
  }

  // Record that the current worker thread has stopped.
  cache_set('stmplease_running', FALSE);
}

/**
 * Retrieve the next message in the mail queue, or the specified message
 * if it has not yet been processed.
 *
 * @param $id
 *   (optional) The mail queue id of a specific message to retrieve.
 *   If not specified, the queue will be processed chronologically.
 */
function _smtplease_get_next_message($id = 0) {
  // If an id has been specified, and it's one we haven't processed, retrieve 
  // it. Otherwise, retrieve the next message in the queue chronologically.
  if (!empty($id)) {
    $mail_data = db_fetch_array(db_query("select * from {smtplease_queue} where id = %d", $id));
  }
  else {
    $mail_data = db_fetch_array(db_query_range("select * from {smtplease_queue} where processed = 0 order by created", 0, 1));
  }

  return $mail_data;
}
