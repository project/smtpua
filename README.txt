-- SUMMARY --

This module enables per-user SMTP authentication.  Users may each specify unique
authentication credentials and susbequently send emails using these credentials.
Alternatively, a server-wide configuration is also supported for anonymous
users, users who have not specified credentials, or site administrators who 
prefer to have SMTP authentication support on a per-server basis only.

Additionally, this module is designed to make the process of sending emails
through your site appear faster.  Traditionally, users sending mail must wait
for mail server communications to complete before being presented with the
next page.  This module has configuration options that allows mail to be queued 
and sent in a separate thread.  Queued mail may be sent during the next cron 
run, or - if the HTTP Parallel Request Library is installed - immediately in new
process that is transparent to the user.

For a full description of the module, visit the project page:
  http://drupal.org/project/smtplease

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/smtplease


-- REQUIREMENTS --

* This module requires the PHP PEAR Mail extension.  See the INSTALL.TXT file 
  for detailed installation instructions.
  
* Optionally, you may also install the HTTP Parallel Request Library to support
  the sending of mail messages in a separate non-blocking process.
  
  http://drupal.org/project/httprl


-- INSTALLATION --

* See the INSTALL.TXT file for special installation instructions


-- CONFIGURATION --

* Module configuration options can be found at /admin/settings/stmplease/default

* Per-user SMTP authentication details can be set at /user/%uid%/edit


-- CONTACT --

Current maintainers:
* Chris Bricker (ISPTrader) - http://drupal.org/user/561630


This project has been sponsored by:

* ISPTrader
    Your trusted source for new and certified pre-owned networking and telecom
    equipment. ISPTrader have clients in every corner of the world, and ships 
    internationally – even to the most difficult locations. ISPTrader’s innovative 
    business model is designed to benefit customers by providing high quality 
    equipment and knowledgeable, personal service at competitive prices.  
  
