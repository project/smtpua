<?php
/**
 * @file
 * Provides administrative configuration functionality for the SMTPlease module.
 */

/**
 * Build administrative settings form.
 *
 * @return array
 *   Returns the administrative settings form structure.
 */
function smtplease_settings_form() {
  $settings = variable_get('smtplease_settings', array());
  $form = array();

  // Check for the existence of the PEAR Mail library and report if missing.
  if (!smtplease_check_include('Mail.php')) {
    drupal_set_message('
      The PHP Mail library is required by the SMTPlease module and was not
      found.  Outgoing SMTP messages will not be sent.  Please refer to the 
      INSTALL.TXT file for installation instructions.
    ', 'error');
  }    

  // Build the admin settings form.
  $form['DefaultServerSettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default SMTP Server Settings'),
    '#description' => t('
      The default server settings are used when per user server configuration
      has not been enabled, or when it has been enabled but is left blank.
    '),
  );

  $form['DefaultServerSettings']['Server'] = array(
    '#type' => 'textfield',
    '#title' => t('SMTP Server Address'),
    '#default_value' => empty($settings['Server']) ? '' : $settings['Server'],
    '#size' => 60,
    '#description' => t('
      Enter the address to your smtp (outgoing) mail server.  For SSL encrypted
      connections, prefix the address with \'ssl://\'.
    '),
  );

  $form['DefaultServerSettings']['Port'] = array(
    '#type' => 'textfield',
    '#title' => t('SMTP Server Port Number'),
    '#default_value' => empty($settings['Port']) ? '' : $settings['Port'],
    '#size' => 5,
    '#description' => t('
      Enter the port number for your smtp (outgoing) mail server.  For SSL
      encrypted connections, ensure you have entered the port number on which
      the server accepts SSL connections.
    '),
  );

  $form['DefaultServerSettings']['Username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 40,
    '#default_value' => empty($settings['Username']) ? '' : $settings['Username'],
  );

  $form['DefaultServerSettings']['Password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#size' => 40,
    '#default_value' => empty($settings['Password']) ? '' : $settings['Password'],
  );

  $form['PerUserSettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Settings'),
  );

  $form['PerUserSettings']['PerUserAccounts'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable per-user SMTP accounts',
    '#default_value' => empty($settings['PerUserAccounts']) ? '' : $settings['PerUserAccounts'],
  );

  $form['PerUserSettings']['PerUserServers'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable per-user SMTP server settings',
    '#default_value' => empty($settings['PerUserServers']) ? '' : $settings['PerUserServers'],
  );

  $form['PerformanceSettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Performance Settings'),
  );

  $form['PerformanceSettings']['BatchSize'] = array(
    '#type' => 'textfield',
    '#title' => 'Batch Size',
    '#default_value' => empty($settings['BatchSize']) ? 20 : $settings['BatchSize'],
    '#size' => 4,
    '#description' => t('
      The maximum number of messages that will be sent in a single process.
    '),
  );

  $form['PerformanceSettings']['QueueMail'] = array(
    '#type' => 'checkbox',
    '#title' => 'Queue mail',
    '#default_value' => empty($settings['QueueMail']) ? '' : $settings['QueueMail'],
    '#description' => t('
      Mail will be queued for sending.  Mail will either be sent in a separate
      thread, or on the next Cron run.
    '),
  );

  $form['PerformanceSettings']['MultiThreading'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable multi-threading',
    '#default_value' => empty($settings['MultiThreading']) ? '' : $settings['MultiThreading'],
    '#description' => t('
      Outgoing smtp mail requests will execute in a separate,
      wait free (non-blocking) process.
    '),
  );

  $form['PerformanceSettings']['ConsolidateThreads'] = array(
    '#type' => 'checkbox',
    '#title' => 'Consolidate multiple threads',
    '#default_value' => empty($settings['ConsolidateThreads']) ? '' : $settings['ConsolidateThreads'],
    '#description' => t('
      Multi-threaded requests will utilize only a single separate process
      for sending queued mail.
    '),
  );

  // Add a custom submit handler.
  $form['#submit'][] = 'smtplease_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Process administrative settings form submittal.
 *
 * @param $form
 *   Administrative settings form structure.
 * @param $form_state
 *   Administrative settings form data.
 */
function smtplease_settings_form_submit($form, &$form_state) {
  $settings = array(
    'Server' => $form_state['values']['Server'],
    'Port' => $form_state['values']['Port'],
    'Username' => $form_state['values']['Username'],
    'Password' => $form_state['values']['Password'],
    'PerUserAccounts' => $form_state['values']['PerUserAccounts'],
    'PerUserServers' => $form_state['values']['PerUserServers'],
    'BatchSize' => $form_state['values']['BatchSize'],
    'QueueMail' => $form_state['values']['QueueMail'],
    'MultiThreading' => $form_state['values']['MultiThreading'],
    'ConsolidateThreads' => $form_state['values']['ConsolidateThreads'],
  );

  // Store the settings.
  variable_set('smtplease_settings', $settings);

  // Clear the already-processed form elements so that they are not further
  // processed by system_settings_form_submit.
  foreach (array_keys($settings) as $field) {
    unset($form_state['values'][$field]);
  }
}


