<?php
/**
 * @file
 *
 * Overrides drupal_send_mail function to provide simple support for
 * SMTP authentication.
 *
 */

/**
 * Implements hook_menu()
 */
function smtplease_menu() {
  $items = array();

  // Add adminsitrative settings menu item.
  $items['admin/settings/smtplease/default'] = array(
    'title' => 'SMTPlease Settings',
    'description' => 'Configure the SMTPlease module',
    'access arguments' => array('administer smtp'),
    'file' => 'smtplease.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('smtplease_settings_form'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  // Add mail worker thread menu item.
  $items['smtplease/send'] = array(
    'title' => 'SMTPlease Mail Worker Thread',
    'description' => 'Process mail queued by SMTPlease',
    'access arguments' => array('access content'),
    'file' => 'smtplease.inc',
    'page callback' => '_smtplease_worker_thread',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_cron()
 */
function stmplease_cron() {
  $settings = variable_get('smtplease_settings', array());

  // If queue mail is enabled, check for messages to be sent. If multi-threading 
  // is enabled, process messages in a separate thread - otherwise process
  // outgoing mail in the current thread.
  if (!empty($settings['QueueMail'])) {
    if (!empty($settings['MultiThreading'])) {
      smtplease_spawn_thread();
    }
    else {
      module_load_include('inc', 'smtplease', 'smtplease');
      _smtplease_worker_thread();
    }
  }

  // Remove the processed message.
  db_query("delete from {smtplease_queue} where processed <> 0");
}

/**
 * Implements hook_form_alter()
 */
function smtplease_form_alter(&$form, &$form_state, $form_id) {
  $settings = variable_get('smtplease_settings', array());

  // Add a section to the User Profile Form if enabled.
  if ('user_profile_form' != $form_id || empty($settings['PerUserAccounts'])) {
    return;
  }

  // Check for existing user settings.
  $user_mail = db_fetch_array(db_query("select * from {smtplease_user} where uid = %d", $form['#uid']));

  // Build user form elements.
  $form['SMTPlease'] = array(
    '#type' => 'fieldset',
    '#title' => t('Outgoing Mail Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['SMTPlease']['id'] = array(
    '#type' => 'hidden',
    '#value' => empty($user_mail['id']) ? 0 : $user_mail['id'],
  );

  $form['SMTPlease']['uid'] = array(
    '#type' => 'hidden',
    '#value' => $form['#uid'],
  );

  $form['SMTPlease']['user_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Email Address'),
    '#default_value' => empty($user_mail['user_address']) ? '' : $user_mail['user_address'],
    '#description' => t('
      Your mail messages will appear as originating from this email address.
    '),
  );

  $form['SMTPlease']['user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Display Name'),
    '#default_value' => empty($user_mail['user_name']) ? '' : $user_mail['user_name'],
    '#description' => t('
      Enter your name as you would like it to appear to email receipients.
    '),
  );

  if (!empty($settings['PerUserServers'])) {
    $form['SMTPlease']['server_address'] = array(
      '#type' => 'textfield',
      '#title' => t('SMTP Server Address'),
      '#default_value' => empty($user_mail['server_address']) ? '' : $user_mail['server_address'],
      '#size' => 60,
      '#description' => t('
        Enter the address to your smtp (outgoing) mail server.  For SSL encrypted
        connections, prefix the address with \'ssl://\'.
      '),
    );

    $form['SMTPlease']['server_port'] = array(
      '#type' => 'textfield',
      '#title' => t('SMTP Server Port Number'),
      '#default_value' => empty($user_mail['server_port']) ? '' : $user_mail['server_port'],
      '#size' => 5,
      '#description' => t('
        Enter the port number for your smtp (outgoing) mail server.  For SSL 
        encrypted connections, ensure you have entered the port number on which 
        the server accepts SSL connections.
      '),
    );

    $form['SMTPlease']['server_username'] = array(
      '#type' => 'textfield',
      '#title' => t('SMTP Server Username'),
      '#size' => 40,
      '#default_value' => empty($user_mail['server_username']) ? '' : $user_mail['server_username'],
    );

    $form['SMTPlease']['server_password'] = array(
      '#type' => 'password',
      '#title' => t('SMTP ServerPassword'),
      '#size' => 40,
      '#default_value' => empty($user_mail['server_password']) ? '' : $user_mail['server_password'],
    );
  }

  $form['#submit'][] = '_smtplease_user_form_submit';

}

/**
 * Add / update per-user mail settings when user profile is updated.
 *
 * @param $form
 *   User profile form structure.
 * @param $form_state
 *   User profile form data.
 */
function _smtplease_user_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['SMTPlease']['id'])) {
    drupal_write_record('smtplease_user', $form_state['values']['SMTPlease'], 'id');
  }
  else {
    drupal_write_record('smtplease_user', $form_state['values']['SMTPlease']);
  }
}

/**
 * Add an outgoing mail message to the queue.
 *
 * @param $message
 *   The mail message array as passed into drupal_mail_wrapper().
 * @param $uid
 *   (optional) The user-id associated with the user sending the message
 *   If not specified, $user->uid will be assumed to be the sender.
 */
function smtplease_queue_mail($message, $uid = null) {
  global $user;

  // Load the module configuration settings.
  $settings = variable_get('smtplease_settings', array());

  // Build the message queue record.
  $queue = array(
    'uid' => empty($uid) ? $user->uid : $uid,
    'message_data' => serialize($message),
    'created' => time(),
  );

  // Commit the message queue record.
  drupal_write_record('smtplease_queue', $queue);

  // Spawn a separate process to send the mail if multi-thresading is enabled.  
  // If thread consolidation is enabled, make sure we don't already have a 
  // thread running.
  if (!empty($settings['MultiThreading'])) {
    if (empty($settings['ConsolidateThreads']) || !cache_get('smtplease_running')) {
      smtplease_spawn_thread($queue['id']);
    }
  }
}

/**
 * Send an outgoing mail message via SMTP using the PEAR Mail library.
 *
 * @param $message
 *   The mail message array as passed into drupal_mail_wrapper().
 * @param $uid
 *   The user-id associated with the user sending the message.
 * @return bool
 *   TRUE if the outgoing mail message was sent successfully.
 *
 */
function smtplease_send_mail($message, $uid) {
  // Load the PEAR Mail library.
  require_once 'Mail.php';

  // Load the module configuration settings.
  $settings = variable_get('smtplease_settings', array());

  // Determine if we have user configured mail parameters.
  $user_data = db_fetch_array(db_query("select * from {smtplease_user} where uid = %d", $uid));

  // Invoke the smtp object with authentication parameters.
  $smtp = Mail::factory('smtp', 
    array(
      'auth'     => TRUE,
      'host'     => empty($user_data['server_address'])  ? $settings['Server']   : $user_data['server_address'],
      'port'     => empty($user_data['server_port'])     ? $settings['Port']     : $user_data['server_port'],
      'username' => empty($user_data['server_username']) ? $settings['Username'] : $user_data['server_username'],
      'password' => empty($user_data['server_password']) ? $settings['Password'] : $user_data['server_password'],
    )
  );

  // Send the mail message.
  $mail = $smtp->send($message['to'], $message['headers'], $message['body']);

  // Report any errors encountered.
  if (PEAR::isError($mail)) {
    $error_message = '<pre>' . print_r($mail->getMessage(), TRUE) . '</pre>';
    watchdog('smtplease', $error_message, WATCHDOG_ERROR);
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Spawn a new thread to send mail via the HTTPRL module / library.
 *
 * @param $id
 *   (optional) The mail queue id of a specific message to send
 */
function smtplease_spawn_thread($id = 0) {
  // Threads are only supported with the presence of the HTTPRL module.
  if (!module_exists('httprl')) {
    return;
  }

  // Build the thread url.
  $url = httprl_build_url_self() . 'smtplease/send/' . $id;

  // Queue and execute the thread request.
  httprl_request($url, array('blocking' => FALSE));
  httprl_send_request();
}

/**
 * Return per-user mail settings for the specified user-id.
 *
 * @param $uid
 *   The user-id associated with the requested user mail settings
 */
function smtplease_get_user_data($uid) {
  $user_data = db_fetch_array(db_query("select * from {smtplease_user} where uid = %d", $uid));
  return $user_data;
}

/**
 * Check if a given file is present in the include path.
 *
 * @param $filename
 *   The name of the file to search for.
 */
function smtplease_check_include($filename) {
  foreach (explode(':', ini_get('include_path')) as $path) {
    if (file_exists($path . '/' . $filename)) {
      return TRUE;
    }
  }
  
  return FALSE;
}

